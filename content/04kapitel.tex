%!TEX root = ../dokumentation.tex

\chapter{Strukturmuster}

% WICHTIG: Beachtung der 4 Aspekte jedes Musters (Name, Problemstellung, Lösung und Konsequenzen)

Strukturmuster (en. Structural Patterns) befassen sich mit dem Aufbau von Klassen und Objekten und dienen, die Objekte und Klassen für größere Systeme nutzbar zu machen.
Es wird zwischen Klassen-basierten Strukturmustern und Objekt-basierten Strukturmustern unterschieden.
Die Klassen-basierten Strukturmuster nutzen Vererbung, um die Klassenfunktionalität zu erweitern, die Objekt-basierten nutzen die Eigenschaften der Objekte direkt.
Der \textit{Adapter} ist das einzige Klassen-basierte Strukturmuster,, welches auch als Objekt-basiertes Muster implementiert werden kann.
Die restlichen Pattern sind Objekt-basiert \cite{gof_1994}.

\section{Adapter}
Der \textit{Adapter} befasst sich mit dem Übertrag von Objektfunktionalität in andere Kontexte.
Für ein maschinelles Lernmodell werden Konfigurationen der Hyperparameter benötigt.
Diese können im JSON-Format vorliegen und werden während der Laufzeit in ein assoziatives Array eingelesen.
Doch das maschinelle Lernmodell kann die Hyperparameter nur als ein spezielles Objekt eines Frameworks, nicht als assoziatives Array einlesen.

Das Objekt-basierte \textit{Adapter} Pattern besteht aus einem \lstinline|Adaptee|, der über den \lstinline|Adapter| für eine Zielklasse bereitgestellt wird. Der \lstinline|Adapter| kann die Daten des \lstinline|Adapters| einlesen und verwenden \cite{gof_1994}.\\
Der Klassen-basierte \textit{Adapter} vererbt von dem \lstinline|Adaptee| direkt, statt nur eine Schnittstelle zu diesem zu sein \cite{gof_1994}.

Der Klassen-basierte \textit{Adapter} ermöglicht neben der Schnittstelle zwischen Target und Adaptee auch, die Funktionalität des Adaptee aufgrund der Vererbung anzupassen.
Für Subklassen des Adaptees ist die Funktionalität nicht gegeben.
Der Objekt-basierte \textit{Adapter} ist durch Methoden auch für Subklassen des Adaptees einsetzbar, kann aber nur schwer die Methoden des Adaptees überschreiben \cite{gof_1994}.

\section{Bridge}
Die \textit{Bridge} befasst sich mit dem Abkoppeln von der Implementation mit der Abstraktion.
Ein E-Mail Programm besteht aus einer Klasse \lstinline|Mailfenster|, welches für die meisten Betriebssysteme verwendet werden soll.
Dazu existieren mehrere Subklassen von \lstinline|Mailfenster|.
Gleichzeitig soll das Fenster für die Benutzer konfigurierbar sein.
Alle zu implementierenden Klassen in eine Hierarchie zu packen, erhöht den Schreibaufwand und erschwert die Wartung.

Das \textit{Bridge} Pattern löst dieses Problem, indem die Implementierung parallel in einer separaten Klassenhierarchie zu der Abstraktion existiert.
Die Abstraktion implementiert ein Objekt der Implementation  und greift so auf die andere Hierarchie zu \cite{gof_1994}.

Durch die Entkopplung der Implementation von der Abstraktion können sich beide Hierarchien parallel entwickeln.
Dadurch haben Änderungen bei der Abstraktion kaum Einfluss auf die Implementation und umgekehrt.
Ebenso kann dadurch Rückkompatibilität bei verschiedenen Versionen garantiert werden \cite{gof_1994}.

\section{Composite}
Das \textit{Composite} befasst sich mit der Kapselung ähnlicher Objekte.
Eine Grafikapplikation bietet die Möglichkeit, Linien und Formen zu zeichnen.
Zum Bewegen der Formen und Linien können Container verwendet werden, welche die Linien und Formen gruppieren und als ein zusammenhängendes Objekt wahrnehmen.
Auch wenn die Container und die Linien/Formen von dem Benutzer gleich behandelt werden, sind sie in der Implementierung unterschiedlich, was Wartbarkeit erschwert.

Das \textit{Composite} Pattern definiert eine Stammklasse, von welcher verschiedene Subklassen plus die \lstinline|Composite| Klasse erbt.
Die \lstinline|Composite| Klasse ist zu der Stammklasse identisch und erlaubt somit die Verschachtelung von \lstinline|Composite| in \lstinline|Composite| \cite{gof_1994}.

Der Vorteil eines \textit{Composites} ist, dass der Client dadurch ein simples und uniformes Objekt verwenden kann.
Ebenso ist es leicht, neue Subklassen hinzuzufügen, ohne dass die Logik der Klassenhierarchie gestört wird \cite{gof_1994}. 

\section{Decorator}
Der \textit{Decorator} befasst sich mit dem dynamischen Anfügen von Funktionalität zu einem Objekt.
Für eine Textapplikation soll ein zu großer Text eine Scrollbar erhalten.
Da ein Text nicht immer groß genug ist, kann die Scrollbar nicht permanent bei einem Text vorhanden sein und somit nicht als Superklasse des Textes existieren.

Das \textit{Decorator} Pattern definiert eine \lstinline|Decorator| Klasse, welche von der zu dekorierenden Klasse erbt.
Der \lstinline|Decorator| akzeptiert ein Objekt der Stammklasse in einer Methode, um die Logik der Methode der Stammklasse für die Logik des \lstinline|Decorators| bereitzustellen \cite{gof_1994}.

Der größte Vorteil des \textit{Decorators} ist die Flexibilität, welche eine Vererbung nicht geben kann.
Weiterhin wird dadurch die Stammklasse nicht verändert.
Gleichzeitig können neue Decorators schnell und unabhängig voneinander implementiert werden \cite{gof_1994}.

\section{Facade}
Die \textit{Facade} befasst sich mit dem Zugriff auf Interfaces eines Subsystems durch eine uniforme Schnittstelle.
Ein Compiler-Programm besteht aus einem Scanner für Token, einem Parser und dem Codegenerator.
Wenn ein anderes Programm auf die Komponenten das Subsystems einzeln zugreift, entstehen dadurch viele Zugriffe und die Notwendigkeit, die Deklaration der Komponenten zu kennen.

Das \textit{Facade} Pattern kapselt die einzelnen Komponenten des Subsystems unter ein uniformes Interface, über welches die Aufrufe auf die anderen Komponenten übertragen werden.
Die \lstinline|Facade| dient als Gateway zwischen dem komplexen Subsystem und dem Client \cite{gof_1994}.

Aufgrund der Kapselung muss der Client keine genauen Implementationsdetails des Subsystems kennen, sondern kommuniziert einzig über die Facade, was die Anzahl der Objekte auf Client-Seite verringert.
Weiterhin entsteht keine starke Abhängigkeit zwischen Client und Implementation des Subsystems, sondern nur zwischen Client und Facade \cite{gof_1994}.

\section{Flyweight}
Das \textit{Flyweight} befasst sich mit dem mehrfachen Nutzen von Objekten zur Reduktion der Objektanzahl.
Für ein Textprogramm wird jeder Buchstabe in ein Objekt gespeichert.
Bei dem doppelten Vorkommen eines Buchstaben liegen nun zwei gleiche Objekte vor, welche sich nur in der Position im Text unterscheiden.
Die Information, was für ein Buchstabe das Objekt erhält, ist redundant.

Das \textit{Flyweight} Pattern löst diese Redundanz, indem Objekte als Flyweight existieren.
Die Flyweights werden über eine Factory bereitgestellt.
Ein Client kann über die Flyweight Factory auf die Flyweights zugreifen, die Factory regelt Objekterstellung und sorgt dafür, dass die gleiche Objekte nicht doppelt instanziiert werden \cite{gof_1994}.

\textit{Flyweights} erhöhen die Laufzeit, reduzieren dafür aber den Speicherverbrauch.
Je nach Anzahl der Flyweights steigt auch der Speicherverbrauch, der durch das Pattern eingespart wird \cite{gof_1994}.

\section{Proxy}
Der \textit{Proxy} steht stellvertretend für ein anderes Objekt und kontrolliert Zugriff zu diesem.
Für eine Webapplikation sollen Bilder auf der Seite durch den Benutzer hinzuzufügen sein.
Der Nutzer kann die Bilder über Links zu diesen bereitstellen.
Manche Bilder können in der Dimension groß sein und somit viel Speicherplatz auf dem Server verwenden.
Ebenso benötigt der Übertrag der Bilder in der HTML-Seite eine hohe Bandbreite.

Das \textit{Proxy} Pattern löst dieses Problem, indem statt einem Objekt ein Proxy-Objekt existiert, welches dieses repräsentiert und Zugriffe auf dieses verwaltet.
Die \lstinline|Proxy| Klasse und die zu vertretende Klasse erben von einer Stammklasse, wobei der Proxy die Methoden der zu vertretenden Klasse aufruft.
Der Client ruft die Methoden des Proxies auf, welcher diese an das vertretende Objekt weiterleitet.
Ein \textit{Proxy} kann in verschiedenen Formen existieren \cite{gof_1994}:
\begin{itemize}
	\item Der \textbf{Remote Proxy} repräsentiert ein objekt, welches außerhalb des Scopes einer Applikation ist
	\item Der \textbf{Virtual Proxy} repräsentiert ein ressourcenintensives Objekt
	\item Der \textbf{Protection Proxy} kontrolliert den Zugriff auf das repräsentierte Objekt
\end{itemize}

Alle Proxy-Varianten dienen dazu, das dahinterliegende Objekt zu maskieren.
Der \textit{Proxy} erlaubt zudem effizienteres Kopieren eines Objekts \cite{gof_1994}.