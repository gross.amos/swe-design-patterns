%!TEX root = ../dokumentation.tex

\chapter{Erstellungsmuster}

% WICHTIG: Beachtung der 4 Aspekte jedes Musters (Name, Problemstellung, Lösung und Konsequenzen)

Erstellungsmuster (en. Creational Patterns) befassen sich mit dem Erstellungsprozess von Objekten und dienen, Systeme unabhängig von spezifischer Objektinstanzierung zu gestalten.
Es wird zwischen Klassen-basierten Erstellungsmustern und Objekt-basierten Erstellungsmustern unterschieden.
Die Klassen-basierten Erstellungsmuster nutzen Vererbung, um die zu erstellenden Objekte zu variieren, die Objekt-basierten delegieren den Erstellungsprozess zu anderen Objekten.
Die \textit{Factory Method} ist das einzige Klassen-basierte Erstellungsmuster, die restlichen sind Objekt-basiert \cite{gof_1994}.

\section{Abstract Factory}
Für einen E-Mail Client ist eine variable Darstellungsform häufig von Nöten, beispielsweise die Darstellungsform von Text oder Fenstern.
Der Nutzer soll die Möglichkeit haben, dass Design des Programms individuell anzupassen.
Wenn eine Änderung in einer festen Darstellung passieren soll, muss ein Entwickler den gesamten Code dafür anpassen.
Ebenso ist ein Hinzufügen von weiteren Designs erschwert.

Das \textit{Abstract Factory} Pattern löst dieses Problem, indem eine Hierarchie für Klassen erstellt wird und der Client nur abstrakte Klassen importiert:
Eine Stammklasse, die \lstinline|Abstract Factory|, deklariert die Methoden für die Subklassen.
Die Subklassen, ebenfalls Factories, sind für die Erstellung eines konkreten Produkts verantwortlich \cite{gof_1994}.

Die \textit{Abstract Factory} ermöglicht es, konkrete Implementierungen der vererbenden Klassen zu isolieren.
Ein großer Vorteil ist, dass konkrete Klassen mit keiner oder geringer Änderung der abstrakten Klasse erstellt werden können.
Weiterhin sind die konkreten Klassen durch die abstrakte Klasse uniform.\\
Nachteilig ist, dass eine Änderung in der \textit{Abstract Factory} auch auf alle Unterklassen passieren muss \cite{gof_1994}.

\section{Builder}
Der \textit{Builder} befasst sich mit der Instanziierung komplexer Objekte.
Bei einem Programm für Konfigurationsmanagement ist es von Nöten, verschiedene Formate einlesen zu können, beispielsweise JSON, XML und YAML.
Für jedes Textformat ein eigenes Objekt mit einem eigenen Konstruktor zu erstellen, ist aufwändig.

Das \textit{Builder} Pattern vereinfacht den Konstruktionsprozess für komplexe Objekte durch eine feste Schnittstelle:
Es existiert eine Stammklassse, der \lstinline|Builder|, von welchem die komplexen Klassen erben und ihre eigene Logik für ihren Anwendungsfall implementieren.
Der \lstinline|Builder| wird dann im Programmablauf von einem \lstinline|Director| verwendet \cite{gof_1994}.

Vorteilhaft bei dem \textit{Builder} Pattern ist die Kapselung von Implementationen des \textit{Builders}.
Erweiterungen sind leicht zu implementieren und bei dem \textit{Builder} muss kein Code angepasst werden \cite{gof_1994}.

\section{Factory Method}
Die \textit{Factory Method} befasst sich mit der Erstellung einer applikationsspezifischen Instanz.
Bei einem Framework zum Dateimanagement existiert eine Klasse \lstinline|Datei| und eine Klasse \lstinline|Dateimanager|. Wenn ein Programmierer eine spezifische Datei mit spezifischen Attributen für das Framework verwenden will, führt das zu einem Problem. Das Framework kennt keine spezifischen Dateien, sondern nur eine allgemeine Repräsentation der Dateien in Form der abstrakten \lstinline|Datei| Klasse.

Das \textit{Factory Method} Pattern liefert dafür eine Lösung, indem von den generellen Klassen geerbt und die Attribute für den Anwendungsfall überschrieben werden.
Der \lstinline|Creator| ist die Klasse, welche das \lstinline|Product| erstellt. Von dem \lstinline|Creator| wird der konkrete Creator für ein konkretes Produkt geerbt und die notwendigen Methoden überschrieben. Das konkrete Produkt erbt von der Stammklasse \lstinline|Product| \cite{gof_1994}.

Die \textit{Factory Method} ermöglicht es, konkrete Klassen außerhalb der eigentlichen Applikation zu halten.
Nachteilhaft ist, dass für den Creator unter Umständen viele Subklassen erstellt werden müssen \cite{gof_1994}. 

\section{Prototype}
Der \textit{Prototype} befasst sich mit der Instanziierung von Stammklassen aus unbekannten Subklassen.
Ein Framework für graphische Oberflächen bietet eine Klasse \lstinline|Icon|. 
\textit{Icon} kann verschiedene Subklassen haben, welche spezifisch durch einen Programmierer implementiert werden können.
Das Problem dabei ist, dass die Stammklasse \lstinline|Icon| von den implementierten Subklassen nicht weiß und das Framework diese nicht verwenden kann.

Das \textit{Prototype} Pattern löst dieses Problem, indem die Subklassen eine Methode der Stammklasse überschreiben. Die Subklassen rufen in der Methode die Methode der Stammklasse auf und können zusätzlich nich eigene Logik implementieren.
Das Framework kann dann mit einer Subklasse arbeiten \cite{gof_1994}.

Vorteilhaft an dem \textit{Prototype} Pattern ist, dass die Implementation der Subklassen von dem Programmablauf gekapselt ist.
Ebenso sind weitere Implementationen leicht umzusetzen, ohne die Stammklasse zu verändern.
Im Vergleich zu der \textit{Factory Method} wird keine parallele Klassenhierarchie aufgebaut, die auch auf Clientseite vorhanden sein muss \cite{gof_1994}.

\section{Singleton}
Das \textit{Singleton} befasst sich mit der Kontrolle der Anzahl von Instanzen einer Klasse.
Für manche Klassen ist es notwendig, dass nur eine begrenzte Anzahl von Instanzen zu einer Zeit im Programm existieren.
Klassen erlauben durch den Konstruktor das Erstellen von mehreren Instanzen.
Eine Möglichkeit der Kontrolle, die allerdings unsicher ist, ist das Verwenden von globalen Variablen.

Das \textit{Singleton} Pattern löst dies, indem die Klasse selbst statisch die Instanzen speichert.
Wenn ein Objekt der Klasse instanziiert werden soll, wird nicht der Konstruktor aufgerufen, sondern eine Methode der Klasse.
Diese Methode implementiert Logik, um die Instanzanzahl zu regulieren und ruft für eine neue Instanz den privaten Konstruktor auf.
Wenn keine neue Instanz erstellt wird, wird eine bereits existierende Instanz zurückgegeben \cite{gof_1994}.

Das Pattern ermöglicht genaue Kontrolle über die Instanzen aufgrund der Kapselung dieser innerhalb der Klasse.
Im Vergleich zu rein statischen Klassen, kann die Anzahl der erlaubten Objekte bei dem \textit{Singleton} auch größer als 1 sein und die Abstraktion der Klasse ist nicht statisch beschränkt \cite{gof_1994}.