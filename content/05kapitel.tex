%!TEX root = ../dokumentation.tex

\chapter{Verhaltensmuster}

% WICHTIG: Beachtung der 4 Aspekte jedes Musters (Name, Problemstellung, Lösung und Konsequenzen)

Verhaltensmuster (en. Behavioral Patterns) befassen sich mit Algorithmen und der Zuweisung von Zuständigkeiten zwischen Objekten.
Dabei wird in Klassen- und Objekt-basierte Verhaltensmuster unterschieden.
Klassen-basierte Verhaltensmuster ordnen Verhalten über ein Vererbungsprinzip zu.
Es werden im Folgenden zwei Klassen-basierte Verhaltensmuster erläutert; die \textit{Template Method} und der \textit{Interpreter}.
Die restlichen Verhaltensmuster sind Objekt-basiert.
Diese verwenden Objektkomposition, um Verhalten zuzuordnen. \cite{gof_1994}

\section{Chain of Responsibility}

Das \textit{Chain of Responsibility} (dt. Verantwortungskette) Design Pattern ist ein Objekt-basiertes Muster, dass sich mit Handlungsketten befasst.
Um die Problemstellung besser zu verstehen, wird das Beispiel eines Online-Shops herangeführt.
In dem Shop wäre es sinnvoll Requests nach dem Eingang zu validieren.
Hierfür könnte beispielsweise ein Handler vor den Aufruf der Requests geschoben werden.
Mit der Zeit könnten alternative Handler, wie bspw. Caching für unkritische Requests dazukommen.
Es bildet sich eine lineare Hierarchie an Handlern.
Jeder Handler bearbeitet unterschiedliche Requests.
Werden die Handler als gewöhnliche Kette implementiert, wo jedes mitglied das nächste aufruft, kann dies zu Problemen führen.
Falls einer der Handler geändert werden muss, muss man potenziell an völig anderer Stelle Änderungen vornehmen.
Zusätzlich sind einzelne Zwischenschritte in die Kette gebunden und können nur schwer wiederverwendet werden \cite{refactoringguru_cor:2023}.

Die vorgeschlagene Lösung des Design Patterns erlaubt es einen Zwischenschritt zu implementieren, ohne genau zu wissen, was davor und danach kommt.
Die Handler werden sodurch entkoppelt. 
Konkrete Handler Objekte erben von einer gemeinsamen \texttt{Handler} Klasse.
Die Handler Klasse definitiert eine gemeinsame Schnittstelle um nachfolgende Handler aufzurufen und extern auf die Handlerlogik zuzugreifen. 
Es wird extern aus Handlern die Ketter erstellt.
So kann außerhalb der Handlerimplementierung die Reihenfolge der Handler definitiert und einzelne Handler nach Bedarf wiederverwendet werden.
Um einen Auftrag zu bearbeiten wird dieser der Kette (bzw. dem ersten Handler) übergeben.
Falls der Auftrag nicht zum einem Handler passt, kann dieser den Nachfolger aufrufen, ohne genau wissen zu müssen welcher dieser ist.
Ein Beispiel wie eine solche Chain of Responsibility implementiert werden könnte ist in Anhanng \ref{apdx:chainofresponsibility} abgebildet.

Das Chain of Responsibility Deign Pattern hat drei größere Konsequenzen.
Erstens reduziert das Muster die Kopplung zwischen Handlern.
Ein Handler kann daher problemlos in mehreren Ketten wiederverwendet werden und Änderungen in einem Handler müssen nicht in anderen Handlern bretractet werden.
Zweitens bietet das Pattern zusätzliche Flexibilität bei der Zuweisung von Zuständigkeiten der Handler.
Die Zuständigkeit und Reinfolge einzelner Handler können zur Laufzeit leicht angepasst werden.
Letztlich bietet das Pattern keine Bearbeitungsgarantie. 
Wird eine Anfrage von allen Handlern ignoriert wird sie nicht bearbeitet. 
\cite{gof_1994}

\section{Command}

Das \textit{Command} (dt. Befehl) Design Pattern ist ein Objekt-basiertes Muster, dass sich mit der Kapselung von Requests in Objekten befasst.
Das Design Pattern widmet sich Fällen, in dem Requests an Objekte gerichtet werden müssen, ohne das etwas über die auszuführende Operation bekannt ist.
Wenn man beispielsweise einen generischen Button implementieren will, muss man beim Knopfdruck eine Funktion auslösen, ohne das bekannt ist, was genau die Funktion ist.

\begin{figure}[H]
    \includegraphics[width=0.9\textwidth]{command.png}
    \centering
    \caption{Beispielanwendung für Commands (entnommen \cite{refactoringguru_cmd:2023})}
    \label{fig:command}
\end{figure}

Das Design Pattern abstrahiert Operationen in Objekten.
Das zentrale Element des Patterns ist eine abstrakte Command-Klasse.
Die Command-Klasse definiert eine gemeinsame Funktion, die aufgerufen werden kann.
Einzelne Operationen erben von der Command-Klasse und überschreiben diese.
Die Objekte können daraufhin an andere Klassen übergeben werden und diese können Operationen ausführen, ohne deren Kontext wissen zu müssen.
Im Buttonbeispiel könnte man so verschiedene Funktionen als Commands auffassen.
Ein Button könnte so verschiedene Operationen ausführen, ohne an bestimmte Implementierungen gebunden zu sein (s. Anhang \ref{apdx:command}).
Wie in Figur \ref{fig:command} dargestellt, könnten auf die gleiche Weise andere Komponenten mit den gleichen Funktionen ausgestattet werden.
Beispielsweise könnte ein Keyboard-Shortcut auf das gleiche Objekt zugreifen.
Das Design Patter verbessert sodurch die wiederverwendbarkeit von Operationen. 
\cite{refactoringguru_cmd:2023}

Das Command Design Pattern entkoppelt den Aufrufer einer Operation von dessen Implementierung.
Besonders, wenn Obekte mit Operationen parametriesiert werden sollen, ergibt es Sinn dieses Design Pattern anzuwenden.
Scheduling und Queues sind weitere sinnvolle anwendungen \cite{refactoringguru_cmd:2023}.
In modernen Sprachen mit Lambda Funktionen ergibt es jedoch wenig Sinn das Pattern anzuwenden.
Lambda Funktionen erlauben in etwa das Gleiche zu erreichen, sind dabei aber syntaktisch und semantisch einfacher zu verwenden.

\section{Interpreter}

Das \textit{Interpreter} Design Pattern befasst sich mit einfachen Gramattiken.
Mit dem Pattern ist es möglich eine gegebene Sprache zu interpretieren und sie entsprechend zu repräsentieren.
Das Design Pattern könnte beispielsweise bei der Implementierung von einfachen Parsern für Sprachen wie \textit{Regex} Sinn ergeben.
In der Praxis findet das Pattern heutszutage kaum Verwendung \cite{oo-interpreter:2006}.

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth]{interpreter-classes.png}
    \centering
    \caption{Klassenhierarchie für das Regex-Beispiel (Quelle: \cite{gof_1994})}
    \label{fig:interpreter-classes}
\end{figure}

Im Design Pattern wird jede Gramattik Regel durch eine eigene Klasse repräsentiert.
Bei der Implementierung einer Regex Funktion, könnte dies beispielsweise Alternierungen, Literale, Sequenzen und Wiederholungen beinhalten (s. Abbildung \ref{fig:interpreter-classes}).
Mit diesen Klassen wird ein \ac{AST} aufgebaut (s. Abbildung \ref{fig:interpreter}).
Beim Einlesen neuer Strings wird mit einer \texttt{interpret()} Funktion geprüft, ob  die Eingabe mit der Gramatikregel übereinstimmt. \cite{gof_1994}

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth]{interpreter.png}
    \centering
    \caption{Beispiel eines ASTs für das Interpreter Pattern}
    \label{fig:interpreter}
\end{figure}

Das Interpreter Design Pattern ist einer der Design Patterns, die kaum verwendet werden \cite{oo-interpreter:2006}.
Es ist hilfreich um Komplexe Grammatiken darzustellen in Fällen, wo Effizienz kein Faktor spielt.
Heutzutage sind statt eigen-implementierten Parsern, Regex Libraries eine bessere Wahl für die Problemstellung. 

\section{Iterator}

Das \textit{Iterator} Design Pattern befasst sich mit dem sequentiellen Zugriff auf die Elemente eines aggregierten Objekts, ohne dessen zugrunde liegende Struktur offenzulegen \cite{gof_1994}.
In Programmen werden Collections unterschiedlichster Art verwendet.
Collections speichern ihre Elemente in unterschidlichsten Daten Strukturen.
Am häufigsten kommen Listen vor.
HashMaps, Bäume und Graphen sind ebenfalls häufig verteten.
Wenn Entwickler*innen auf alle Elemente einer Collection zugreifen wollen, dann muss die Datenstruktur manuell durchkämmt werden.
Falls sich die Struktur ändert, muss der komplette Algorithmus neu entworfen werden.
\cite{refactoringguru:2023}

Das Pattern versucht dieses Problem zu lösen.
Es wird für eine Collection ein neues Itterator Objekt erzeugt.
Das Itterator Objekt hat zwei Funktionen \texttt{getNext()} und \texttt{hasMore()} mit welchen die vollständige Collection durchgekämmt werden kann.
Für unterschiedliche Traversal Algorithmen können verschiedene Itterator-Objekte erzeugt werden. 
\cite{gof_1994}

Es ergibt Sinn das Pattern zu verwenden, wenn eine Collection eine Komplexe Datenstruktur besitzt und die Schnittstelle diese Komplexität nicht vorweisen soll.
Zusätzlich kann es sinvoll sein das Pattern zu verwenden um Traversal Code nicht duplizieren zu müssen \cite{gof_1994}.
Das Pattern ist in vielen Sprachen, wie Java miteingebaut und kann nativ verwendet werden (s. Anhang \ref{apdx:iterator})

\section{Mediator}

Das \textit{Mediator} (dt. Vermittler) Design Pattern vereinfacht komplexe Beziehungsnetze zwischen mehreren Objekten. 
Bei einem Text Editor könnten beispielsweise Klassen für Textfelder, Auswahllisten und Checkboxen voneinander abhängig sein.
So könnte beispielsweise die Auswahl einer Font Auswirkungen auf die Verfügbarkeit von Stylings wie Fettdruck und des Aussehens des Textes im Textfeld haben.
Sobald viele solche Beziehungen existieren, entsteht ein Netz an eng gekoppelten Klassen.
Elemente an anderen Stellen wiederzuverwenden wird erheblich schwieriger.
\cite{gof_1994}

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth]{mediator.png}
    \centering
    \caption{Kommunikationsbild mit und ohne eines Mediators}
    \label{fig:mediator}
\end{figure}

Mit dem Mediator Pattern wird dieses Netz aufgelöst.
Statt, dass alle Klassen miteinander kommunizieren müssen, wird eine Mediator Klasse gewählt, über welche die Klassen indirekt kommunizieren.
Im Text Editor Beispiel würde das Bedeuten, dass das Textfeld nicht mehr den Status des Fontfeldes und der Checkbox überprüfen muss, bevor es ein stück text Fett markiert.
Es müsste einfach eine einzige Anfrage an den Mediator stellen (s. Abbildung \ref{fig:mediator}).
Direkte Kommunikation wird mit indirekter Kollaboration ersetzt.
Die Abhängigkeiten der verschiedenen Klassen wurden sodurch erheblich vereinfacht.
Wird zusatzlich über ein Interface mit dem Mediator kommuniziert, kann die Kopplung der Klassen mit dem Mediator weiter verringert werden.
\cite{gof_1994}

Das Mediator Design Pattern besitzt, ähnlich wie die restlichen Muster, einige Konsequenzen die beachtet werden müssen.
Zum einen werden die Klassen durch das Muster entkoppelt.
Zum anderen wird die von n-zu-n Interktionen zu 1-zu-n Interkationen abgestuft.
Letztlich wird die Objektkooperation zu einem bestimmten grad abstrahiert, was deren modularität verbessert.
\cite{gof_1994}


\section{Memento}

Das \textit{Memento} Design Pattern befasst sich mit dem Exteralisieren und Wiederherstellen von internen Zuständen, ohne dessen Kapselung zu beinträchtigen.
In einem Editor muss für eine Undo Funktion beispielsweise der Zustand des Editierfeldes externalisiert werden, sodass dieser Später wiederhergestellt werden kann.
Hier sollte jedoch externen nicht Zugriff auf alle internen Variablen gegeben werden, da dies die Kapselung der Klassen aufheben würde.
\cite{gof_1994}

Dieses Problem lässt sich mit dem Memento Pattern lösen.
Ein Mememto ist ein Objekt, das den internen Zustand eines anderen Objekts (dem Urheber, en. Originator) speichert.
Der Urheber kann daraufhin mit einem Mememto initialisiert werden, um den Stand wiederherzustellen.
Das Mememto kann daraufhin an eine andere Klasse übergeben und verwaltet werden (Verwalter, en. Caretaker).
Die Beziehung des Verwalters, zum Memento und dem Urheber ist in Abbildung \ref{fig:memento} abgebildet.
\cite{gof_1994}

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth]{memento.png}
    \centering
    \caption{Mememto, Urheber und Verwalter (Quelle: \cite{gof_1994})}
    \label{fig:memento}
\end{figure}

Die Verwendung von Mememtos trägt zur Beibehaltung der Kapselung bei.
Die Verwaltung der Mementos kann jedoch sehr aufwendig sein, wenn der Urheber große Mengen an Informationen kopien muss, um ein Memento zu erstellen.
Zudem kann dieser Mehraufwand schnell verdeckt werden, da dem Verwalter die Komplexität des Urhebers versteckt wird.
\cite{gof_1994}

\section{Observer}

Das \textit{Observer} (dt. Beobachter) Design Pattern befasst sich mit der Benachrichtigung von Objekten bei Zustandsänderungen anderer Objekte \cite{gof_1994}.
Falls beispielsweise in einer Tabellenkalculation mehrere Spalten abhängig von einer Anderen sind, kann es notwendig sein, die abhängigen Spalten bei Änderungen anzupassen.
Besonders bei großen Tabellen kann es schnell unhandbar werden, alle Spalten zu aktualisieren.
Was also benötigt wird, ist ein Weg um abhängige Objekte bei der Änderungen eines anderen Objektes zu benachrichtigen.

Das Pattern unterscheidet in zwei Objekttypen; Subjekte und Observer.
Ein Subjekt kann beliebig viele abhängige Observer haben.
Ändert sich das Subjekt werden alle Observer benachrichtigt.
Diese Benachrichtigung wird durch den Aufruf einer gemeinsamen Notifictaion Funktion in den Observer Objekten implementiert.
In modernen Sprachen sind hierbei auch Lambda-Funktionen beliebt.
Jeder Observer kontaktiert daraufhin das Subjekt, um den eigenen Zustand anzupassen.
In anderen Kontexten ist dieses Pattern ebenfalls als \textit{Publish-Subscribe} bekannt.
Eine mögliche Implementierung des Patterns ist in Anhang \ref{apdx:observer} zu finden.
\cite{gof_1994}

Das Observer Pattern hat zur Konsequenz, dass das Subjekt und Observer entkoppelt werden.
Das Subjekt hat keine Kentnis über die konkrete Klasse des Observers.
Zum anderen haben einzelne Observer kein Wissen über andere Observer.
Eine kleine Änderung im Subjekt kann so zu einer unerwarteten Lawine an Benrichtigungen führen.
\cite{gof_1994}

\section{State}

Das \textit{State} (dt. Zustand) Design Pattern erlaubt einem Objekt das Verhalten zu ändern, wie als wenn das Objekt die Klasse geändert hätte. 
Als Beispiel wird eine TCP-Connection Klasse verwendet.
Ein TCP-Connection Objekt kann sich in mehreren Zuständen befinden; verbunden (en. connected), horchend (en. listening) oder getrennt (en. closed).
Das Verhalten des Objekts ist abhängig von diesen Stati.
\cite{gof_1994}

Mithilfe des State Patterns kann das Verhalten in diesen Stati modeliert werden. 
Der äußeren Klasse wird ein State Attribut hinzugefügt.
Die Funktionen der äußeren Klasse verweisen intern auf Funktionen die im State Attribut implementiert sind.
Objekte des State Attributs Teilen sich das gleiche Interface, implementieren aber die genauen Funktionen selber.
Falls das Verhalten der äußeren Klasse geändert werden soll, kann einfach das State Attribut ausgetauscht werden.
\cite{gof_1994}

Die Nutzung des Design Patterns hat genau drei Konsequenzen.
Erstens ordnet es Zuständen bestimmte Verhaltensweisen zu.
Zweitens werden Zustandsänderungen im Quellcode deutlich hervorgehoben.
Es ist als Entwickler kaum möglich nicht die einzelnen Zustände zu beachten.
Drittens können State Objekte (falls sie keine intene Instanzvariablen besitzen) geteilt werden und produzieren daher kaum einen Overhead. 
\cite{gof_1994}

\section{Strategy}

Das \textit{Stategy} (dt. Strategie) Design Pattern befasst sich mit austauschbaren Algorithmen \cite{gof_1994}.
In vielen Fällen ist es hilfreich verschiedene Algorithmen für eine Funktion anzubieten und diese zur Laufzeit ändern zu können.
Ein Beispiel wäre die Sortierung einer Online-Bibliothek.
Hier könnten von Nutzern unterschiedliche Sortierung erwünscht sein, bspw. nach Autor, Titel oder Genre.
Für jede Sortierung muss eine andere Funktion angewendet werden.
Ansonsten bleibt jedoch die unterliegende Klasse gleich.

Das Pattern besteht aus zwei wesentlichen Teilen, einem Kontext und einer Strategie.
Die Strategie ist eine abstrakte Klasse, die durch konkrete Strategien implementiert werden muss.
Der Kontext besitzt eine Referenz auf ein Strategie-Objekt, und kann eine Schnittstelle bereitstellen, die es der Strategie erlaubt auf Kontextdaten zuzugreifen.
Der Kontext verwendet darauhin den in der Strategie definitierten Algorithmus.
\cite{gof_1994}

Vor der Verwendung des Strategie Design Patterns müssen Entwickler*innen über gewisse Konsequenzen bescheid wissen.
Ersten können durch Vererbung verschiedene Strategie-Klassen zu einer Familie von Algorithmen und Verhaltensweisen gruppiert werden.
Zweitens kann durch Vererbung der Kontextklasse ein ähnlicher Effekt erziehlt werden.
Besonders, wenn die Strategie nicht zur Laufzeit ausgetauscht werden muss, kann Vererbung eine einfachere Alternative darstellen.
Drittens entsteht durch die zusätzliche Kommunkation zwische Strategie und Context ein gewisser Overhead.
\cite{gof_1994}

\section{Template Method}

Das \textit{Template Method} (dt. Schablonenmethode) Design Pattern ermöglicht es Klassen die Grundstruktur eines Algorithmus festzulegen und anschließend Unterklassen Spezifika zu überschreiben \cite{gof_1994}.
Dies könnte beispielsweise in einem Messagepassing System benötigt werden, dass viele Protokolle unterstützen soll.
Damit der gemeinsame Code nicht dupliziert wird, müssten bei einer Implementierung für jeden Protokollunterschied Konditionale eingebaut werden.
Eine solche Implementierung könnte schnell unübersichtlich und unnötig Komplex werden.

\begin{figure}[H]
    \includegraphics[width=0.7\textwidth]{template-method.png}
    \centering
    \caption{Struktur des Template Method Design Patterns}
    \label{fig:template-method}
\end{figure}

Wesentlich besser wäre es die Protokoll-Spezifikas zu gruppieren und die Grundstruktur zu externalisieren.
Das Template Method Design Pattern nimmt sich genau dies vor.
In dem Pattern definiert eine Abstrakte Klasse die einzelnen Schritte des Algorithmus, sowie eine Template Method, die die Grundstruktur des Algorithmus beschreibt.
Unterklassen überschreiben anschließend die primitiven Operationen (s. Figur \ref{fig:template-method}).
\cite{gof_1994}

Template Methods gehöhren zu einer der wichtigsten Methoden um wiederverendbaren Code zu gewährleisten.
Besonders in Bibliotheken ist das Design Pattern beliebt.
Zudem Methods ist es entscheidend, dass es für Entwickler*innen eindeutig ist, welche Methoden überschrieben werden dürfen.
\cite{gof_1994}


\section{Visitor}

Das \textit{Visitor} (dt. Besucher) Design Pattern ermöglicht die Definition von neuen Operationen ohne, dass die konkreten Klassen der Objekte bekannt sind.
Als Beispielanwendung könnte man einen JSON Exporter für Geodaten sehen.
Verschiedene Nodes der Geodaten sind durch unterschiedliche Klassen dargestellt.
Den Export Code in diese Klassen zu integrieren würde das Single Responsibility Prinzip verletzen und ist daher abzuraten.
\cite{gof_1994}

Eine bessere Alternative stellt das Vistor Pattern dar.
Im Visitor Pattern gibt es zwei größere Interfaces; Visitor und Visitable.
Das Visitor Interface definiert die Schnittstelle, mit der das Pattern via einer \texttt{visit} angewendet werden soll.
Konkrete Funktionen werden in Klassen implementiert, die von diesem Interface erben.
Im Beispiel wäre das eine Implementierung für den JSON exporter.
Das Visitable interface definiert eine \texttt{accept()} Funktion, die ein Visitable annimmt und die \texttt{visit} Funktion des Visitors ausführt.
Klassen, auf die das Pattern angewendet werden soll (im Beispiel Geodaten Nodes), implementieren das Visitable Interface.
\cite{gof_1994}

Das Visitor Patter ermöglicht es neue Operationen auf Objekte zu definieren, ohne dass dabei das Objekt angepasst werden muss.
Es hilft zudem bei der Trennung von Funktionen und vereinheitlicht ähnliche Operationen.
Ein Problem ist, dass es das Hinzufügen neuer Unterklassen von Elementen (im Beispiel Geodaten Nodes) erschwert, da es in der Visitor Implementierung berücksichtigt werden muss.
Zudem kann mit dem Ansatz oft die Kapselung verletzt werden, da angenommen wird, dass für die zu implementierende Funktion die öffentliche Schnittstelle genügt.
Dies ist nicht immer der Fall.
\cite{gof_1994}


