#! /bin/bash

for dir in behavioural creational structural; do
    cd "$dir" || { echo "Directory $dir does not exist"; exit 1; }
    javac *.java
    cd ..
done


for dir in behavioural creational structural; do
    echo -e "\n--- $dir ---"
    for file in "$dir"/*.java; do
        classname=$(basename "$file" .java)
        echo -e "\nExecuting $classname..."
        java -cp "$dir" "$classname"
    done
done