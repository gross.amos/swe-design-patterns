interface Implementation {
    void operation();
}

class ConcreteImplementationA implements Implementation {
    @Override
    public void operation() {
        System.out.println("ConcreteImplementationA: operation");
    }
}

class ConcreteImplementationB implements Implementation {
    @Override
    public void operation() {
        System.out.println("ConcreteImplementationB: operation");
    }
}

abstract class Abstraction {
    protected Implementation implementation;

    public Abstraction(Implementation implementation) {
        this.implementation = implementation;
    }

    public abstract void operation();
}

class RefinedAbstraction extends Abstraction {
    public RefinedAbstraction(Implementation implementation) {
        super(implementation);
    }

    @Override
    public void operation() {
        System.out.println("RefinedAbstraction: operation");
        implementation.operation();
    }
}

public class BridgePattern {
    public static void main(String[] args) {
        Implementation implementationA = new ConcreteImplementationA();
        Abstraction abstractionA = new RefinedAbstraction(implementationA);
        abstractionA.operation();

        Implementation implementationB = new ConcreteImplementationB();
        Abstraction abstractionB = new RefinedAbstraction(implementationB);
        abstractionB.operation();
    }
}
