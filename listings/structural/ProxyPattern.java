interface Image {
	
    public void displayImage();
}

// class, that is very heavy and shouldn't be instantiated in the code, if not necessary
class RealImage implements Image {
	
    private String filename;
	
    public RealImage(String filename) {
        this.filename = filename;
    }

    public void displayImage() {
        System.out.println("Displaying " + filename);
    }
}

class ProxyImage implements Image {
	
    private String filename;
    private RealImage image;
    
    public ProxyImage(String filename) {
        this.filename = filename;
    }

    public void displayImage() {
        if (image == null) {
           image = new RealImage(filename);
        }
        image.displayImage();
    }
}

public class ProxyPattern {
  
   public static void main(String[] arguments) {
        Image image = new ProxyImage("image.jpg");

        image.displayImage();
    }
}