interface Printable {
	void print();
}

class Printer implements Printable {
	
	@Override
	public void print() {
		System.out.println("printing done");
	}
}

abstract class PrinterDecorator implements Printable {
	
	private Printer printer;
	
	public PrinterDecorator(Printer printer) {
		this.printer = printer;
	}
	
	@Override
	public void print() {
		printer.print();
	}
}

class StarPrinterDecorator extends PrinterDecorator {
	
	public StarPrinterDecorator(Printer printer) {
		super(printer);
	}
	
	@Override
	public void print() {
		System.out.println("***********");
		super.print();
		System.out.println("***********");
	}
}

public class DecoratorPattern {
	public static void main(String[] args) {
		StarPrinterDecorator star = new StarPrinterDecorator(new Printer());
		star.print();
	}
}