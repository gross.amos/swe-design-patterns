
import java.util.ArrayList;

class Observer {
    public void update(Subject subject) {
        System.out.println("recieved state: " + subject.getState());
    }
}

class Subject {
    private int state;
    private ArrayList<Observer> observers = new ArrayList<>();

    public void setState(int state) {
        this.state = state;
        for (Observer o : observers) {
            o.update(this);
        }
    }

    public int getState() {
        return state;
    }

    public void subscribe(Observer observer) {
        observers.add(observer);
    }

    public void unsubscribe(Observer observer) {
        observers.remove(observer);
    }
}

public class ObserverPattern {
    public static void main(String[] args) {
        Subject subject = new Subject();
        Observer observer = new Observer();

        subject.subscribe(observer);

        subject.setState(7);
    }
}