
abstract class Command {
    public abstract void execute();
}

class SaveCommand extends Command {
    public void execute() {
        System.out.println("command saving");
    } 
}

class UndoCommand extends Command {
    public void execute() {
        System.out.println("command undo");
    } 
}

class Button {
    private Command command;
    public void setCommand(Command command) {
        this.command = command;
    }
    public void onClick() {
        command.execute();
    }
}

public abstract class CommandPattern {
    public static void main(String[] args) {
        Command undo = new UndoCommand();
        Command save = new SaveCommand();

        Button button1 = new Button();
        Button button2 = new Button();
        button1.setCommand(undo);
        button2.setCommand(save);
        
        button1.onClick();
        button2.onClick();
    }
}