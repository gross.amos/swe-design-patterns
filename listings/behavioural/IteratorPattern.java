
import java.util.Iterator;

class MyCollection<T> implements Iterable<T> {
    T myval;

    public MyCollection(T val) {
        this.myval = val;
    }

    public Iterator<T> iterator() {
        return new MyIterator<T>(this);
    }

    public T getMyval() {
        return myval;
    }
}

class MyIterator<T> implements Iterator<T> {
    MyCollection<T> iter;

    public MyIterator(MyCollection<T> iter) {
        this.iter = iter;
    }

    public boolean hasNext() {
        return true;
    }

    public T next() {
        return iter.getMyval();
    }
}

public class IteratorPattern {
    public static void main(String[] args) {
        MyCollection<Integer> myCollection = new MyCollection<>(3);

        int counter = 3;
        for (int val : myCollection) {
            System.out.println(val);
            counter -= 1;
            if (counter <= 0) {
                break;
            }
        }
    }
}