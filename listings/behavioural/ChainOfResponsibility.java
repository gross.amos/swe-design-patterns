abstract class Handler {
    protected Handler successor;
    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }
    public abstract void handleNumber(int mynum);
}

class ConcreteHandlerA extends Handler {
    public void handleNumber(int mynum) {
        if (mynum == 2) {
            System.out.println(mynum + " in Handler A");
        } else {
            super.successor.handleNumber(mynum);
        }
    }
}

class ConcreteHandlerB extends Handler {
    public void handleNumber(int mynum) {
        if (mynum != 2) {
            System.out.println(mynum + " in Handler B");
        } else {
            super.successor.handleNumber(mynum);
        }
    }
}

public class ChainOfResponsibility {
    public static void main(String[] args) {
        Handler h1 = new ConcreteHandlerA();
        Handler h2 = new ConcreteHandlerB();
        h1.setSuccessor(h2);

        h1.handleNumber(2);
        h1.handleNumber(5);
    }
}